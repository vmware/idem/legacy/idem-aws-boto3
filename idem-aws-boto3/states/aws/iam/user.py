from typing import Any, Dict


async def state(hub, ctx, **kwargs):
    username: str = kwargs["username"]
    path: str = kwargs.get("path", "/")

    success, ret = await hub.exec.aws.iam.list_users(ctx, path_prefix=path)
    if success is not True:
        return success, ret

    for user in ret:
        if user["UserName"] == username:
            return success, {k: str(v) for k, v in user.items()}

    return success, {}


async def present(hub, ctx, **kwargs) -> Dict[str, Any]:
    # Get the arguments for the state
    username: str = kwargs["username"]

    if ctx.before:
        ctx.ret.result = True
        ctx.ret.comment = f"User {username} already exists"
        # TODO check for path and other attributes being correct
    else:
        if ctx.test:
            ctx.ret.result = True
            ctx.ret.comment = f"User {username} would be created"
        else:
            ctx.ret.result, result = await hub.exec.aws.iam.create_user(ctx, username=username)
            if not ctx.ret.result:
                ctx.ret.comment = result
            else:
                ctx.ret.comment = f"User {username} was created"

    return ctx.ret


async def absent(hub, ctx, **kwargs) -> Dict[str, Any]:
    # Get the arguments for the state
    username: str = kwargs["username"]

    if ctx.before:
        # User exists, delete it
        if ctx.test:
            ctx.ret.comment = "User would be deleted"
        else:
            ctx.ret.result, result = await hub.exec.aws.iam.delete_user(ctx, username=username)
            if ctx.ret.result:
                ctx.ret.comment = "User deleted."
            else:
                ctx.ret.comment = result
    else:
        ctx.ret.result = True
        ctx.ret.comment = f"User {username} is already absent"

    return ctx.ret
