from typing import Any, Dict


async def state(hub, ctx, **kwargs):
    groupname: str = kwargs["groupname"]
    path: str = kwargs["path"] if "path" in kwargs else "/"
    success, group_list = await hub.exec.aws.iam.list_groups(ctx)
    if not success:
        ctx.ret.result = False
        ctx.ret.comment = group_list
        return ctx.ret

    for group in group_list:
        if "GroupName" in group and group["GroupName"] == groupname:
            if "Path" in group and group["Path"] == path:
                return True, group
    return True, {}


async def present(hub, ctx, **kwargs) -> Dict[str, Any]:
    groupname: str = kwargs["groupname"]
    path: str = kwargs.get("path", "/")

    if ctx.before:
        ctx.ret.result = True
        ctx.ret.comment = f"Group {groupname} (path {path}) already exists."
        return ctx.ret

    if ctx.test:
        ctx.ret.result = True
        ctx.ret.comment = "Group would be created"
        return ctx.ret

    success, resp_obj = await hub.exec.aws.iam.create_group(ctx, groupname=groupname, path=path)
    if success:
        ctx.ret.result = True
        ctx.ret.comment = f"Group {groupname} (path {path}) created."
    else:
        ctx.ret.result = False
        ctx.ret.comment = resp_obj

    return ctx.ret


async def absent(hub, ctx, **kwargs) -> Dict[str, Any]:

    """
    Ensure the specified group does not exist
    """

    groupname: str = kwargs["groupname"]

    if ctx.before:
        if ctx.test:
            ctx.ret.result = True
            ctx.ret.comment = "Group would be deleted"
            return ctx.ret

        success, resp_obj = await hub.exec.aws.iam.delete_group(ctx, groupname=groupname)
        if success:
            ctx.ret.result = True
            ctx.ret.comment = f"Group {groupname} deleted."
        else:
            ctx.ret.result = False
            ctx.ret.comment = resp_obj
    else:
        ctx.ret.result = True
        ctx.ret.comment = f"Group {groupname} already absent."
    return ctx.ret
