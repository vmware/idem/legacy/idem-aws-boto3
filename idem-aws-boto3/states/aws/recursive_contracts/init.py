import copy
from dict_tools import data as dict_tools
from dict_tools import differ as dict_differ
from typing import Any, Dict


async def sig_present(hub, ctx, **kwargs) -> Dict[str, Any]:
    ...


async def sig_absent(hub, ctx, **kwargs) -> Dict[str, Any]:
    ...


async def sig_state(hub, ctx, **kwargs) -> Any:
    """
    Get the current state of a resource.
    This is used by the "call" contract to generate a diff of the changes
    It's output will be injected into every state's "before" argument
    """


async def call_state(hub, ctx, **kwargs) -> Any:
    """
    Override caller so that the "state" function doesn't expect a "before" injection

    # TODO this could cache results
    #    if no changes are made after a state call then use cached results next time this function is called
    #    On changes, the call contract would update the cache with it's "after" data
    """
    success, ret = await ctx.func(*ctx.args, **ctx.kwargs)
    # Verify that the state return type can be deep_diffed
    # This is only useful for development and could be disabled for production
    assert hasattr(ret, "__iter__")
    assert all(hasattr(k, "__hash__") for k in ret)
    return success, ret


async def call(hub, ctx) -> Dict[str, Any]:
    kwargs = ctx.get_arguments()
    func_hub = kwargs.pop("hub")
    func_ctx = copy.copy(kwargs.pop("ctx"))
    func_kwargs = kwargs.pop("kwargs")
    func_name = func_kwargs.pop("name")
    ret = dict_tools.NamespaceDict(name=func_name, result=False, changes={}, comment="")
    func_ctx.ret = ret

    state_func = getattr(hub.states, f"aws.{ctx.ref}").state
    success, before = await state_func(func_ctx, **func_kwargs)
    if success is not True:
        ret.comment = before
        return ret

    func_ctx.before = before
    result = await ctx.func(func_hub, func_ctx, **func_kwargs)
    if result is not None:
        ret.update(result)

    success, after = await state_func(func_ctx, **func_kwargs)
    if success is not True:
        ret.result = False
        ret.comment = after
        return ret

    ret.changes = dict_differ.deep_diff(before, after)
    return ret
