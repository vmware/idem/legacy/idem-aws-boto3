#####################################################################################################
# Wrapper Functions -- Used at end of code to dynamically generate wrappers.
#####################################################################################################

# CamelCase names that aren't simple conversions. Typically involves extra capitalization.

camel_munge = {"username": "UserName", "new_username": "NewUserName", "ssh_public_key_id": "SSHPublicKeyId"}


def _camelify(arg):
    """
	Convert argument foo_bar_oni to AWS CamelCase format: "FooBarOni".

	Will also apply any special name mappings in `camel_munge`.

	This function will fail with IndexError if passed an arg with a trailing "_". Don't do that. No AWS
	variables are formatted like that anyway.
	"""
    if arg in camel_munge:
        return camel_munge[arg]
    arg = arg[0].upper() + arg[1:]
    while True:
        underpos = arg.find("_")
        if underpos == -1:
            break
        arg = arg[:underpos] + arg[underpos + 1].upper() + arg[underpos + 2 :]

    return arg


def _aws_tagdict(tags):
    """
	Converts a Python dictionary into a format that AWS expects for tags.
	"""
    aws_tags = []
    for key, val in tags:
        aws_tags.append({"Key": key, "Value": val})
    return aws_tags


def _adapt_args(kwargs_dict: dict, adapters: dict = None):
    """
	Convert a dictionary of key:value pairs received from an idem exec function so that all keys are in AWS camelcase,
	and return this new dict, plus perform some useful bonus functions detailed below.

	Bonus functions
	===============

	Adapters: values can be tweaked by supplying a dictionary of lambdas for the `adapters` keyword argument. Lambdas
	should be indexed based on the matching key in `camelcase_args`. If such a lambda is found, the value of the dict
	will be passed to the function for further pre-processing.


	"""
    if adapters is None:
        adapters = {}
    adapted_kwargs = {}
    for arg, val in kwargs_dict.items():
        if arg in adapters:
            adapted_kwargs[_camelify(arg)] = adapters[arg](val)
        else:
            adapted_kwargs[_camelify(arg)] = val

    return adapted_kwargs


def wrap_simple_api_call(hub, svc, api_call, adapters: dict = None, expected_kwargs: set = None, optional_kwargs: set = None):
    if adapters is None:
        adapters = {}
    if expected_kwargs is None:
        expected_kwargs = set()
    if optional_kwargs is None:
        optional_kwargs = set()

    async def wrapper(ctx, **kwargs):

        # 1. Validate passed keyword arguments. Since we are using **kwargs here, this function will allow any keyword
        #    arguments to be passed to it. AWS will actually give us a decent error message if we are missing arguments
        #    but the error will have the AWS CamelCase versions of the arguments. Better user experience to show the
        #    actual idem exec kwargs we are expecting with underscores.

        unexpected_kwargs = set(kwargs.keys()) - (expected_kwargs | optional_kwargs)
        if unexpected_kwargs:
            raise TypeError(f"{api_call}() got unexpected keyword argument(s): {', '.join(map(lambda x: chr(39) + x + chr(39), unexpected_kwargs))}")

        missing_required_kwargs = expected_kwargs - set(kwargs.keys())
        if missing_required_kwargs:
            raise TypeError(f"{api_call}() requires the following argument(s): {', '.join(map(lambda x: chr(39) + x + chr(39), missing_required_kwargs))}")

        # 2. Call the underlying method, after "adapting" our exec function passed kwargs (convert to camelcase and
        #    possibly tweak values using adapters:

        outval = _adapt_args(kwargs, adapters=adapters)
        return await hub.tool.aws.common.simple_api_call(ctx, svc, api_call, **_adapt_args(kwargs, adapters=adapters))

    return wrapper


def wrap_engine(hub, svc, methods, out):
    for api_call, params in methods.items():

        if isinstance(params, tuple):
            # If passed a tuple, the second element of the tuple is a dict of things that gives us optional arguments
            # as well as lambdas to adapt values.
            params, extras = params
        else:
            extras = {}

        # Optional parameters do not need to be in the main list of required parameters:

        if "optional" in extras:
            optional_kwargs_set = set(extras["optional"])
        else:
            optional_kwargs_set = set()

        if "adapt" in extras:
            adapters = extras["adapt"]
        else:
            adapters = {}

        # If an optional argument is listed in the main arg list and optional, make sure we don't require it:
        expected_kwargs_set = set(params) - optional_kwargs_set

        wrapper = wrap_simple_api_call(hub, svc, api_call, adapters=adapters, expected_kwargs=expected_kwargs_set, optional_kwargs=optional_kwargs_set)
        out[api_call] = wrapper
